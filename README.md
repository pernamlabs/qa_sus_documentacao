# Repositório de documentos sobre o fluxo de QA/Sustentação

Disponibilizamos o fluxograma do processo de QA/Sustentação aqui no arquivo **"Fluxo-do-Processo-de-QA"**. Também está disponível o documento que ilustra o gerenciamento do fluxo de SCM, o nome do mesmo é **"Fluxo_de_SCM"**.

Aqui você vai encontrar informações importantes como por exemplo: guias de projetos, descrição do fluxo de sustentação e informações sobre componentes; isso seguimentado nas branches "catalogo", "farol_debito_tecnico", "guide_automacao_api" e "guide_infra".

**Branch - catalogo:**
  Catálogo de componentes a ser preenchido pelo dono do projeto, será utilizado na session com o time de sustentação.
  
**Branch - farol_debito_tecnico:**
  Planilha utilizada para compor o dashboard do farol de débito técnico.
  
**Branch - guide_automacao_api:**
  Guia com práticas para desenvolvimento de automação de casos de testes em API.
  
**Branch - guide_infra:**
  Guia de boas práticas em configuração de Deploy, assim como de implementação e segurança da API.
  
**Branch - guide_teste_carga:**
  Guia com práticas para configuração de testes de carga/stress. 